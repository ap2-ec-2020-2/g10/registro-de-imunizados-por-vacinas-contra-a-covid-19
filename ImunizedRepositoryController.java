import javax.swing.JComponent;
import javax.swing.JOptionPane;
import java.util.List;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.GregorianCalendar;
public class ImunizedRepositoryController
{
    public ImunizedRepository model;
    public ImunizedRepositoryView view;
    public ImunizedRepositoryController(ImunizedRepository model, ImunizedRepositoryView view)
    {
        this.model = model;
        this.view = view;
        init();
    }   
    public void init() {
        model.setState(ImunizedRepository.State.INITIAL);
        view.getImunizedButtonFormPanel().getBCreate().addActionListener(e -> creating());
        view.getImunizedButtonFormPanel().getBOk().addActionListener(e -> commit());
        view.getImunizedButtonFormPanel().getBCancel().addActionListener(e -> cancel());
        view.getImunizedButtonFormPanel().getBSearch().addActionListener(e -> searching());
        view.getImunizedButtonFormPanel().getBFirst().addActionListener(e -> loading("FIRST"));
        view.getImunizedButtonFormPanel().getBPrevious().addActionListener(e -> loading("PREVIOUS"));
        view.getImunizedButtonFormPanel().getBNext().addActionListener(e -> loading("NEXT"));
        view.getImunizedButtonFormPanel().getBLast().addActionListener(e -> loading("LAST"));
        view.getImunizedButtonFormPanel().getBUpdate().addActionListener(e -> updating());
        view.getImunizedButtonFormPanel().getBDelete().addActionListener(e -> deleting());
        updateView();
    } 
    private void enableComponents(List<JComponent> components) {
        for (JComponent component : components) {
            component.setEnabled(true);
        }
    } 
    private void disableComponents(List<JComponent> components) {
        for (JComponent component : components) {
            component.setEnabled(false);
        }
    }
    private void clearFields (List<JComponent> components){
        for(JComponent component : components) {
            ((javax.swing.JTextField) component).setText("");
        }
    }   
    public void updateView () {
        if (model.getState() == ImunizedRepository.State.INITIAL){
            enableComponents(view.getImunizedButtonFormPanel().getSearchCreateButtons());
            disableComponents(view.getImunizedButtonFormPanel().getNavigationButtons());
            disableComponents(view.getImunizedButtonFormPanel().getOkCancelButtons());
            disableComponents(view.getImunizedButtonFormPanel().getDeleteUpdateButtons());
            clearFields(view.getImunizedFormularyPanel().getAllFields());
            disableComponents(view.getImunizedFormularyPanel().getAllFields());
        } else if (model.getState() == ImunizedRepository.State.CREATING){
            enableComponents(view.getImunizedButtonFormPanel().getOkCancelButtons());
            disableComponents(view.getImunizedButtonFormPanel().getNavigationButtons());
            disableComponents(view.getImunizedButtonFormPanel().getSearchCreateButtons());
            disableComponents(view.getImunizedButtonFormPanel().getDeleteUpdateButtons());
            enableComponents(view.getImunizedFormularyPanel().getAllFields());
            view.getImunizedFormularyPanel().getTFId().setEnabled(false);
        } else if(model.getState() == ImunizedRepository.State.SEARCHING) {
            enableComponents(view.getImunizedButtonFormPanel().getOkCancelButtons());
            disableComponents(view.getImunizedButtonFormPanel().getNavigationButtons());
            disableComponents(view.getImunizedButtonFormPanel().getDeleteUpdateButtons());
            disableComponents(view.getImunizedButtonFormPanel().getSearchCreateButtons());
            disableComponents(view.getImunizedFormularyPanel().getAllFields());
            view.getImunizedFormularyPanel().getTFId().setEnabled(true);
            clearFields(view.getImunizedFormularyPanel().getAllFields());
        }else if ( ImunizedRepository.LOADED.contains(model.getState())) {
            try{
                loadFromModel();
            }catch (Exception e) {
                JOptionPane.showMessageDialog(view, "Error: Repository not loaded. Details: "+e);
            }
            disableComponents(view.getImunizedButtonFormPanel().getSearchCreateButtons());
            disableComponents(view.getImunizedButtonFormPanel().getOkCancelButtons());
            enableComponents(view.getImunizedButtonFormPanel().getNavigationButtons());
            enableComponents(view.getImunizedButtonFormPanel().getDeleteUpdateButtons());
            disableComponents(view.getImunizedFormularyPanel().getAllFields());
            view.getImunizedButtonFormPanel().getBCancel().setEnabled(true);
            if(model.getState() == ImunizedRepository.State.LOADED_ONE){
                disableComponents(view.getImunizedButtonFormPanel().getNavigationButtons());
            }else if(model.getState() == ImunizedRepository.State.LOADED_FIRST){
                view.getImunizedButtonFormPanel().getBFirst().setEnabled(false);
                view.getImunizedButtonFormPanel().getBPrevious().setEnabled(false);
            }else if(model.getState() == ImunizedRepository.State.LOADED_LAST){
                view.getImunizedButtonFormPanel().getBNext().setEnabled(false);
                view.getImunizedButtonFormPanel().getBLast().setEnabled(false);
            }
        }else if(model.getState() == ImunizedRepository.State.UPDATING){
            enableComponents(view.getImunizedButtonFormPanel().getOkCancelButtons());
            disableComponents(view.getImunizedButtonFormPanel().getSearchCreateButtons());
            disableComponents(view.getImunizedButtonFormPanel().getNavigationButtons());
            disableComponents(view.getImunizedButtonFormPanel().getDeleteUpdateButtons());
            enableComponents(view.getImunizedFormularyPanel().getAllFields());
            view.getImunizedFormularyPanel().getTFId().setEnabled(false);
        }else if(model.getState() == ImunizedRepository.State.DELETING){
            enableComponents(view.getImunizedButtonFormPanel().getOkCancelButtons());
            disableComponents(view.getImunizedButtonFormPanel().getSearchCreateButtons());
            disableComponents(view.getImunizedButtonFormPanel().getNavigationButtons());
            disableComponents(view.getImunizedButtonFormPanel().getDeleteUpdateButtons());
            disableComponents(view.getImunizedFormularyPanel().getAllFields());
        }
    }   
    public void creating() {
        model.setState(ImunizedRepository.State.CREATING);
        updateView();
    }    
    public void searching(){
        model.setState(ImunizedRepository.State.SEARCHING);
        updateView();
    }
    public void loading(String event){
        try{
            switch(event) {
                case "PREVIOUS":
                model.loadPrevious();
                if(model.loadedFirst()){
                    model.setState(ImunizedRepository.State.LOADED_FIRST);
                }
                else
                   model.setState(ImunizedRepository.State.LOADED_MIDDLE);
                break;
                case "NEXT":
                model.loadNext();
                if(model.loadedLast()){
                    model.setState(ImunizedRepository.State.LOADED_LAST);
                }
                else
                    model.setState(ImunizedRepository.State.LOADED_MIDDLE);
                break;
                case "LAST":
                   model.loadLast();
                   model.setState(ImunizedRepository.State.LOADED_LAST);
                break;
                case"FIRST":
                   model.loadFirst();
                   model.setState(ImunizedRepository.State.LOADED_FIRST);
                break;
            }
        }catch(Exception e){
            System.out.println("Error on loading. Details: "+e);
        }
        updateView();
    }
    public void updating() {
        model.setState(ImunizedRepository.State.UPDATING);
        updateView();
    }
    public void deleting(){
        model.setState(ImunizedRepository.State.DELETING);
        updateView();
    }
    public void commit(){
        if(model.getState() == ImunizedRepository.State.CREATING){
            try{
                Imunized imunizedLoaded = loadFromView();
                Imunized newImunized = model.create(imunizedLoaded);
                model.setState(ImunizedRepository.State.LOADED_ONE);
                updateView();
            }catch (Exception e){
                JOptionPane.showMessageDialog(view, "Error: user not Saved. Details: " +e);
            }
        } else if(model.getState() == ImunizedRepository.State.SEARCHING){
            try{
                Imunized imunizedLoaded = loadFromView();
                if( imunizedLoaded.getId() == 0){
                    List<Imunized> imunizedsFound = model.loadAll();
                    if (imunizedsFound.size() == 1) {
                        JOptionPane.showMessageDialog(view, "User Found: " + imunizedsFound.size() + " row");
                        model.setState(ImunizedRepository.State.LOADED_ONE);
                        updateView();
                    }else if( imunizedsFound.size() > 1){
                        JOptionPane.showMessageDialog(view, "User Found: " + imunizedsFound.size() + " row");
                        model.setState(ImunizedRepository.State.LOADED_FIRST);
                        updateView();
                    }else 
                        JOptionPane.showMessageDialog(view, "Users not found, repository is empty");
                }else if(imunizedLoaded.getId() > 0){
                    Imunized imunizedFound = model.loadFromId(imunizedLoaded.getId());
                    if(imunizedFound != null){
                        JOptionPane.showMessageDialog(view, "User Found with id: " + imunizedFound.getId());
                        model.setState(ImunizedRepository.State.LOADED_ONE);
                        updateView();
                    }
                    else{
                        JOptionPane.showMessageDialog(view, "User not found");
                    }
                }
            }catch (Exception e){
                JOptionPane.showMessageDialog(view, "Error in the search. Details: " + e);
            }
        }else if(model.getState() == ImunizedRepository.State.UPDATING){
            try{
                Imunized imunizedLoaded = updateToView();
                Imunized newImunized = model.update(imunizedLoaded);
                JOptionPane.showMessageDialog(view, "Imunized updated successfuly");
                model.setState(ImunizedRepository.State.INITIAL);
                updateView();
            }catch(Exception e) {
                JOptionPane.showMessageDialog(view,"Error: Student not updated. Details: " + e);
            }
        }else if(model.getState() == ImunizedRepository.State.DELETING){
            try{
                 Imunized imunizedLoaded = loadToDelete();
                 Imunized newImunized = model.delete(imunizedLoaded);
                 JOptionPane.showMessageDialog(view, "Imunized deleted successfuly");
                 model.setState(ImunizedRepository.State.INITIAL);
                 updateView();
            }catch(Exception e){
                JOptionPane.showMessageDialog(view,"Error: imunized not deleted. Details: " + e);
            }
        }
    }
    public void cancel(){
        if((model.getState() == ImunizedRepository.State.CREATING) || (model.getState() == ImunizedRepository.State.LOADED_ONE) || (model.getState() == ImunizedRepository.State.LOADED_FIRST) || (model.getState() == ImunizedRepository.State.LOADED_MIDDLE) || (model.getState() == ImunizedRepository.State.LOADED_LAST)){
            try{
                int response = JOptionPane.showConfirmDialog(view, "Do you want to cancel and clear all fields?");
                if(response == JOptionPane.YES_OPTION) {
                    model.setState(ImunizedRepository.State.INITIAL);
                    updateView();
                }
            }catch (Exception e) {
                JOptionPane.showMessageDialog(view, "Error: user not saved. Details: " + e);
            }
        } else if( model.getState() == ImunizedRepository.State.SEARCHING){
            model.setState(ImunizedRepository.State.INITIAL);
            updateView();
        }else if( (model.getState() == ImunizedRepository.State.UPDATING) || (model.getState() == ImunizedRepository.State.DELETING)){
            try{
                Imunized imunizedLoaded = loadFromModel();
                List<Imunized> imunizedsLoaded = model.getLoadedImunizeds();
                if(imunizedsLoaded.indexOf(imunizedLoaded) == 0 && imunizedsLoaded.size() == 1){
                    model.setState(ImunizedRepository.State.LOADED_ONE);
                    updateView();
                } else if (imunizedsLoaded.indexOf(imunizedLoaded) == 0 && imunizedsLoaded.size() > 1){
                    model.setState(ImunizedRepository.State.LOADED_FIRST);
                    updateView();
                }else if (imunizedsLoaded.indexOf(imunizedLoaded) > 0 && imunizedsLoaded.indexOf(imunizedLoaded) < (imunizedsLoaded.size() - 1)){
                    model.setState(ImunizedRepository.State.LOADED_MIDDLE);
                    updateView();
                }else if(imunizedsLoaded.indexOf(imunizedLoaded) == (imunizedsLoaded.size()- 1)){
                    model.setState(ImunizedRepository.State.LOADED_LAST);
                    updateView();
                }
            }catch(Exception e){
                JOptionPane.showMessageDialog(view, "Error: user not saved. Details: " + e);
            }
        }
    }   
    private String dateToString( java.util.Date date){
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(date);
    }   
    private java.util.Date stringToDate (String string) throws Exception {
        if (string.equals (""))
            return null;
        Pattern dataPadrao = Pattern.compile("^([0-9]{2})/([0-9]{2})/([0-9]{4})$");
        Matcher matcher = dataPadrao.matcher(string); 
        if (matcher.matches()) {
          int dia = Integer.parseInt(matcher.group(1));
          int mes = Integer.parseInt(matcher.group(2));
          int ano = Integer.parseInt(matcher.group(3));
          return (new GregorianCalendar(ano,mes,dia)).getTime();
        }
        else throw new Exception("Invalid data format"); 
    }   
    private int stringToInt(String string){
        return string.equals("") ? 0 : Integer.parseInt(string);
    }  
    private Imunized loadFromView() throws Exception {
        Imunized imunizedLoaded = new Imunized();
        String strId = view.getImunizedFormularyPanel().getTFId().getText();
        int id = stringToInt(strId);
        imunizedLoaded.setId(id);
        imunizedLoaded.setFullName(view.getImunizedFormularyPanel().getTFFullName().getText());
        imunizedLoaded.setCity(view.getImunizedFormularyPanel().getTFCity().getText());
        imunizedLoaded.setDateOfVaccination(stringToDate(view.getImunizedFormularyPanel().getTFDateOfVaccination().getText()));
        return imunizedLoaded;
    }  
    private Imunized updateToView() throws Exception {
        Imunized imunizedLoaded = model.getLoadedImunized();
        imunizedLoaded.setFullName(view.getImunizedFormularyPanel().getTFFullName().getText());
        view.getImunizedFormularyPanel().getTFFullName().setText(imunizedLoaded.getFullName());
        imunizedLoaded.setCity(view.getImunizedFormularyPanel().getTFCity().getText());
        view.getImunizedFormularyPanel().getTFCity().setText(imunizedLoaded.getCity());
        imunizedLoaded.setDateOfVaccination(stringToDate(view.getImunizedFormularyPanel().getTFDateOfVaccination().getText()));
        view.getImunizedFormularyPanel().getTFDateOfVaccination().setText(dateToString(imunizedLoaded.getDateOfVaccination()));
        String strId = view.getImunizedFormularyPanel().getTFId().getText();
        int id = stringToInt(strId);
        imunizedLoaded.setId(id);
        view.getImunizedFormularyPanel().getTFId().setText(String.valueOf(imunizedLoaded.getId()));
        return imunizedLoaded;
    }  
    public Imunized loadFromModel() throws Exception {
        Imunized imunizedLoaded = model.getLoadedImunized();
        if (imunizedLoaded == null)
            throw new Exception ("Imunized not loaded");
        view.getImunizedFormularyPanel().getTFId().setText(String.valueOf(imunizedLoaded.getId()));
        view.getImunizedFormularyPanel().getTFFullName().setText(imunizedLoaded.getFullName());
        view.getImunizedFormularyPanel().getTFCity().setText(imunizedLoaded.getCity());
        view.getImunizedFormularyPanel().getTFDateOfVaccination().setText(dateToString(imunizedLoaded.getDateOfVaccination()));
        List<Imunized> imunizedsLoaded = model.getLoadedImunizeds();
        if(imunizedsLoaded == null)
           throw new Exception("Repository is not loaded");
        return imunizedLoaded;
    }  
    public Imunized loadToDelete() throws Exception{
        Imunized imunizedLoaded = model.getLoadedImunized();
        if (imunizedLoaded == null)
            throw new Exception ("Imunized not loaded");
        view.getImunizedFormularyPanel().getTFId().setText(String.valueOf(imunizedLoaded.getId()));
        view.getImunizedFormularyPanel().getTFFullName().setText(imunizedLoaded.getFullName());
        view.getImunizedFormularyPanel().getTFCity().setText(imunizedLoaded.getCity());
        view.getImunizedFormularyPanel().getTFDateOfVaccination().setText(dateToString(imunizedLoaded.getDateOfVaccination()));
        return imunizedLoaded;
    }  
    public static void main (String[] args){
        ImunizedRepository model = new ImunizedRepository(new Database(args[0]));
        ImunizedRepositoryView view = new ImunizedRepositoryView();
        ImunizedRepositoryController imunizedRepositoryController = new ImunizedRepositoryController(model, view);
        javax.swing.JFrame jFrame = new javax.swing.JFrame();
        jFrame.setSize(900,600);
        jFrame.setLayout(new java.awt.BorderLayout());
        jFrame.add(view, java.awt.BorderLayout.CENTER);
        jFrame.setVisible(true);
    }  
    public static ImunizedRepositoryController createController(Database database) {
        ImunizedRepositoryView view = new ImunizedRepositoryView();
        ImunizedRepository model = new ImunizedRepository(database);
        ImunizedRepositoryController controller = new ImunizedRepositoryController(model, view);
        return controller;
  }

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie model*/
    public ImunizedRepository getModel(){
        return this.model;
    }//end method getModel

    /**SET Method Propertie model*/
    public void setModel(ImunizedRepository model){
        this.model = model;
    }//end method setModel

    /**GET Method Propertie view*/
    public ImunizedRepositoryView getView(){
        return this.view;
    }//end method getView

    /**SET Method Propertie view*/
    public void setView(ImunizedRepositoryView view){
        this.view = view;
    }//end method setView

    //End GetterSetterExtension Source Code
//!
}
