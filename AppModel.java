
/**
 * Escreva a descrição da classe AppModel aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
import java.io.File;
public class AppModel
{
    private File file;
    private Database database;
    public enum Status{DB_CLOSED, DB_OPEN;}
    private Status status;

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie file*/
    public File getFile(){
        return this.file;
    }//end method getFile

    /**SET Method Propertie file*/
    public void setFile(File file){
        this.file = file;
    }//end method setFile

    /**GET Method Propertie database*/
    public Database getDatabase(){
        return this.database;
    }//end method getDatabase

    /**SET Method Propertie database*/
    public void setDatabase(Database database){
        this.database = database;
    }//end method setDatabase

    /**GET Method Propertie status*/
    public Status getStatus(){
        return this.status;
    }//end method getStatus

    /**SET Method Propertie status*/
    public void setStatus(Status status){
        this.status = status;
    }//end method setStatus

    //End GetterSetterExtension Source Code
//!

    //Start GetterSetterExtension Source Code
    //End GetterSetterExtension Source Code
//!

    //Start GetterSetterExtension Source Code
    //End GetterSetterExtension Source Code
//!
}
