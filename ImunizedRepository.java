import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.EnumSet;

public class ImunizedRepository
{
    private static Database database;
    private static Dao<Imunized, Integer> dao;
    private List <Imunized> loadedImunizeds;
    private Imunized loadedImunized;
    public enum State {INITIAL, SEARCHING, CREATING, DELETING, UPDATING, LOADED_ONE, LOADED_FIRST, LOADED_MIDDLE, LOADED_LAST;}
    public static Set<State> LOADED = EnumSet.of(State.LOADED_ONE, State.LOADED_FIRST, State.LOADED_LAST, State.LOADED_MIDDLE);
    private State state = State.INITIAL;
    
    public ImunizedRepository(Database database){
        ImunizedRepository.setDatabase(database);
        loadedImunizeds = new ArrayList<Imunized>();
    }
    
    public static void setDatabase(Database database){
        ImunizedRepository.database = database;
        try{
            dao = DaoManager.createDao(database.getConnection(), Imunized.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Imunized.class);
        }catch (SQLException e){
            System.out.println(e);
        }
    }
    
    public Imunized create(Imunized imunized) {
        int nrows = 0;
        try {
            nrows = dao.create(imunized);
            if ( nrows == 0 )
                throw new SQLException("Error: object not saved");
            setLoadedImunized(imunized);
            loadedImunizeds.add(imunized);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return imunized;
    }   

    public Imunized update(Imunized imunized) {
        try{
            int nrows = dao.update(imunized);
            if (nrows != 1)
                throw new SQLException("Error: object not updated");
            setLoadedImunized(imunized);
            this.loadedImunizeds.add(this.loadedImunized);
        }catch (SQLException e) {
            System.out.println(e);
        }
        return this.loadedImunized;
    }
    
    public Imunized delete(Imunized imunized) {
        try{
            int nrows = dao.deleteById(imunized.getId());
            if (nrows != 1)
                throw new SQLException("Error: object not deleted");
            this.loadedImunized = imunized;
            this.loadedImunized = null;
            this.loadedImunizeds.remove(imunized);
        }catch (SQLException e){
            System.out.println(e);
        }
        return this.loadedImunized;
    }
    
    public Imunized loadFromId(int id) {
        try {
            this.loadedImunized = dao.queryForId(id);
            if (this.loadedImunized != null)
                this.loadedImunizeds.add(this.loadedImunized);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.loadedImunized;
    }
    
    public List<Imunized> loadAll() {
        try {
            this.loadedImunizeds =  dao.queryForAll();
            if (this.loadedImunizeds.size() != 0)
                this.loadedImunized = this.loadedImunizeds.get(0);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.loadedImunizeds;
    }
    
    public void loadFirst() throws Exception {
       if ( loadedImunized == null || loadedImunizeds == null || loadedImunizeds.size() == 0){
            throw new Exception ("Imunizeds not loaded");
       }
       setLoadedImunized(loadedImunizeds.get(0));
    }
    
    public void loadPrevious() throws Exception {
        if ( loadedImunized == null || loadedImunizeds == null || loadedImunizeds.size() == 0){
            throw new Exception ("Imunizeds not loaded");
        }
        int index = loadedImunizeds.indexOf(loadedImunized);
        if(index == 0)
           throw new Exception("Could not loaded previous imunized, current imunized alredy is the first");
        setLoadedImunized(loadedImunizeds.get(index-1));
    }
    
    public void loadNext() throws Exception{
         if ( loadedImunized == null || loadedImunizeds == null || loadedImunizeds.size() == 0){
            throw new Exception ("Imunizeds not loaded");
        }
        int index = loadedImunizeds.indexOf(loadedImunized);
        if(index == loadedImunizeds.size()-1)
           throw new Exception("Could not loaded next imunized, curent index is the last");
        setLoadedImunized(loadedImunizeds.get(index+1));
    }
    
    public void loadLast() throws Exception{
        if ( loadedImunized == null || loadedImunizeds == null || loadedImunizeds.size() == 0){
            throw new Exception ("Imunizeds not loaded");
        }
        int last = loadedImunizeds.size()-1;
        setLoadedImunized(loadedImunizeds.get(last));        
    }
    
    public boolean loadedLast() throws Exception{
        if ( loadedImunized == null || loadedImunizeds == null || loadedImunizeds.size() == 0){
            throw new Exception ("Imunizeds not loaded");
        }
        if(loadedImunizeds.indexOf(loadedImunized) + 1 == loadedImunizeds.size())
           return true;
        else
           return false;
    }
    
    public boolean loadedFirst() throws Exception{
        if ( loadedImunized == null || loadedImunizeds == null || loadedImunizeds.size() == 0){
            throw new Exception ("Imunizeds not loaded");
        }
        if(loadedImunizeds.indexOf(loadedImunized) == 0)
           return true;
        else
           return false;
    }
    
    //Start GetterSetterExtension Source Code
    /**GET Method Propertie database*/
    public Database getDatabase(){
        return this.database;
    }//end method getDatabase
    
    /**GET Method Propertie dao*/
    public com.j256.ormlite.dao.Dao<Imunized, java.lang.Integer> getDao(){
        return this.dao;
    }//end method getDao

    /**SET Method Propertie dao*/
    public void setDao(com.j256.ormlite.dao.Dao<Imunized, java.lang.Integer> dao){
        this.dao = dao;
    }//end method setDao

    /**GET Method Propertie loadedImunizeds*/
    public java.util.List<Imunized> getLoadedImunizeds(){
        return this.loadedImunizeds;
    }//end method getLoadedImunizeds

    /**SET Method Propertie loadedImunizeds*/
    public void setLoadedImunizeds(java.util.List<Imunized> loadedImunizeds){
        this.loadedImunizeds = loadedImunizeds;
    }//end method setLoadedImunizeds

    /**GET Method Propertie loadedImunized*/
    public Imunized getLoadedImunized(){
        return this.loadedImunized;
    }//end method getLoadedImunized

    /**SET Method Propertie loadedImunized*/
    public void setLoadedImunized(Imunized loadedImunized){
        this.loadedImunized = loadedImunized;
    }//end method setLoadedImunized

    //End GetterSetterExtension Source Code
//!

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie LOADED*/
    public java.util.Set<State> getLOADED(){
        return this.LOADED;
    }//end method getLOADED

    /**SET Method Propertie LOADED*/
    public void setLOADED(java.util.Set<State> LOADED){
        this.LOADED = LOADED;
    }//end method setLOADED

    /**GET Method Propertie state*/
    public State getState(){
        return this.state;
    }//end method getState

    /**SET Method Propertie state*/
    public void setState(State state){
        this.state = state;
    }//end method setState

    //End GetterSetterExtension Source Code
//!
}
