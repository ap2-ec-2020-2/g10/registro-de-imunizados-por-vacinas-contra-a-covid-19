<h1 align="center">Registro de Imunizados por Vacinas contra a COVID-19</h1>


<h2 align="center">Objetivo do projeto:</h2>

<p align="center">Este projeto tem por finalidade a utilização de um registro de pessoas que foram vacinadas contra a Covid-19.
<p align="center">Uma possível função seria integrar o projeto ao sistema Google Maps.</p>

<h2 align="center">VERSÃO:</h2>

<p align="center">Protótipo para o Projeto Final (Algoritmos e Programação 2)</p>

<h2 align="center">Como iniciar o projeto:</h2>

<p align="center">Com a coleta de dados e informações, pode-se traçar análises sobre o perfil de quem foi vacinado, as localidades com os maiores números de pessoas imunizados e também facilitar a identificação dos pacientes, tendo mais controle sobre o sistema de vacinação.</p>

<h2 align="center">Participantes</h2>

<p align="center">[Wilson Conegundes de Freitas Filho](https://gitlab.com/wilsoncf)</p>

<p align="center">[Pedro Siade Ferreira](https://gitlab.com/pedrosiade)</p>

<p align="center">[João Pedro dos Santos Nunes](https://gitlab.com/nunesjoao)</p>

<p align="center">[Pedro Henrique Fernandes Ribeiro](https://gitlab.com/nativop)</p>

<p align="center">[Gustavo Peraro Rodovalho](https://gitlab.com/peraro)</p>

<h2 align="center">Instruções do Projeto:</h2>

<p align="center">• PARA ADICIONAR UM REGISTRO:</p> 
<p align="center"> Clique em NEW > Adicione o NOME, a CIDADE e a DATA DE VACINAÇÃO do paciente > Clique em OK > Logo após, um ID é gerado e salvo.</p>

<p align="center">• PARA BUSCAR UM REGISTRO:
<p align="center">Informe o nome ou ID > Clique em BUSCAR.</p>

<p align="center">• PARA ALTERAR UM REGISTRO:</p>
<p align="center">Pesquise o cadastro desejado > Clique em OK > Clique em EDITAR > Altere a informação desejada > Clique em OK.</p>

<p align="center">• PARA EXCLUIR UM REGISTRO:</p>
<p align="center">Pesquise o cadastro desejado > Clique em OK > Escolha o cadastro utilizando os cursores > Clique em EXCLUIR.</p>
