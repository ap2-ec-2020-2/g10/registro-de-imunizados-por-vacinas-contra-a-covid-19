import java.util.Date;
import java.text.SimpleDateFormat;
import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;

@DatabaseTable(tableName = "Imunized")
public class Imunized
{
    @DatabaseField(generatedId = true)
    private int id;
    
    @DatabaseField
    private String fullName;
    
    @DatabaseField
    private String city;
    
    @DatabaseField(dataType = DataType.DATE)
    public Date dateOfVaccination;
    
    public String printdateOfVaccination() {
        SimpleDateFormat dateFor = new SimpleDateFormat("dd/MM/yyyy");
        return dateFor.format(dateOfVaccination);
    }

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie id*/
    public int getId(){
        return this.id;
    }//end method getId

    /**SET Method Propertie id*/
    public void setId(int id){
        this.id = id;
    }//end method setId

    /**GET Method Propertie fullName*/
    public String getFullName(){
        return this.fullName;
    }//end method getFullName

    /**SET Method Propertie fullName*/
    public void setFullName(String fullName){
        this.fullName = fullName;
    }//end method setFullName

    /**GET Method Propertie city*/
    public String getCity(){
        return this.city;
    }//end method getCity

    /**SET Method Propertie city*/
    public void setCity(String city){
        this.city = city;
    }//end method setCity

    /**GET Method Propertie dateOfVaccination*/
    public Date getDateOfVaccination(){
        return this.dateOfVaccination;
    }//end method getDateOfVaccination

    /**SET Method Propertie dateOfVaccination*/
    public void setDateOfVaccination(Date dateOfVaccination){
        this.dateOfVaccination = dateOfVaccination;
    }//end method setDateOfVaccination

    //End GetterSetterExtension Source Code
//!
}
