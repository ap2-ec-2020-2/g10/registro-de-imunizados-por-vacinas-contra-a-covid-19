
/**
 * Escreva a descrição da classe AppController aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
import javax.swing.JFileChooser;
public class AppController
{
    private AppModel model;
    private AppView view;
    
    public AppController (AppModel model, AppView view){
        this.model = model;
        this.view = view;
        init();
    }
    
    public void init(){
        setStatus(AppModel.Status.DB_CLOSED);
        view.getMIImunized().addActionListener(e -> openImunizedRepository());
        view.getMINew().addActionListener(e -> fileNew());
        view.getMIOpen().addActionListener(e -> fileOpen());
    }
    
    public void setStatus(AppModel.Status status){
        if(status == AppModel.Status.DB_CLOSED){
            view.getMRepository().setEnabled(false);
            view.getMIOpen().setEnabled(true);
            view.getMINew().setEnabled(true);
        }else if (status == AppModel.Status.DB_OPEN){
            view.getMRepository().setEnabled(true);
            view.getMIOpen().setEnabled(false);
            view.getMINew().setEnabled(false);
        }
    }
    
    public void openImunizedRepository() {
        ImunizedRepositoryController imunizedRepositoryController = ImunizedRepositoryController.createController(model.getDatabase());
        view.getTBPane().addTab("Imunized", imunizedRepositoryController.getView());
    }
    
    private void openDatabase() {
        model.setFile(view.getJFileChooser().getSelectedFile());
        view.getLBFIle().setText("File: "+model.getFile().getPath());
        Database database = new Database(model.getFile().getPath());
        model.setDatabase(database);
        setStatus(AppModel.Status.DB_OPEN);
    }
    
    public void fileOpen() {
        int returnVal = view.getJFileChooser().showOpenDialog(view);
        if (returnVal == javax.swing.JFileChooser.APPROVE_OPTION) {
            openDatabase();
        } else {
            view.getLBFIle().setText("Open file operation cancelled");
        }
    }
    
    public void fileNew() {
        int returnVal = view.getJFileChooser().showSaveDialog(view);
        if (returnVal == javax.swing.JFileChooser.APPROVE_OPTION) {
            openDatabase();
        } else {
            view.getLBFIle().setText("New file operation cancelled");
        }
    }
    
    public static void main(String[] args) {
        AppModel model = new AppModel();
        AppView view = new AppView();
        AppController ac = new AppController(model, view);
        view.setVisible(true);
    }
}
